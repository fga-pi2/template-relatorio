# ScanPoint

## Equipe

| Engenharia | Cargo | Membro | Matrícula |
| :- | :- | :- | :--: |
| Software | **Coordenadora geral** | Carla Rocha | 170085023 |
| Software | **Diretora de qualidade** | Brenda Santos | 180041444 |
| Software | **Diretor técnico de Software** | Denniel William | 170161871 |
| Aeroespacial | **Diretora técnica de Estrutura** | Maria Claudia | 200041193 |
| Energia | **Diretora técnica de Eletrônica/Energia** | Carolina Oliveira | 190011483 | 
| Aeroespacial | Desenvolvedor | Cássio Filho | 190011521 |
| Automotiva | Desenvolvedor | Diogo Soares | 190012188 |
| Eletrônica | Desenvolvedor | Miguel Munoz | 221008795 |
| Energia | Desenvolvedor | Lucas Pantoja | 190060352 |
| Software | Desenvolvedora | Ana Carolina | 190101792 |
| Software | Desenvolvedor | Guilherme Basilio | 160007615 |
| Software | Desenvolvedor | Artur de Souza | 190010606 |
| Software | Desenvolvedor | Ciro Costa | 190011611 |
| Software | Desenvolvedor | Vinicius Vieira | 190118059 |
| Software | Desenvolvedor | Pedro Menezes | 190139323 |

## Grupo

<img src="./assets/fotoequipeScan.jpeg" alt="imagem1" width="500" height="380">

## Site

https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-08/scanpoint/

## Nosso produto (clique na imagem para ver o vídeo)

[![](http://img.youtube.com/vi/v-eVb3_VNGw/0.jpg)](https://www.youtube.com/watch?v=v-eVb3_VNGw "Video comercial scanpoint")

## Banner
[Projeto ScanPoint](https://www.canva.com/design/DAGJR7PKIO4/qaewzKEAcOWhN0B7CRS70w/view?utm_content=DAGJR7PKIO4&utm_campaign=designshare&utm_medium=link&utm_source=editor)

## Como rodar os documentos
[Como rodar os documentos](./how-to-docs.md)

## Reuniões

| Dia | Horário |
| :--: | :--: |
| Quarta-feira | 16:00 - 18:00 |
| Sexta-feira | 14:00 - 18:00 |

## Entregas

### C1

Engloba as fases 1, 2 e 3 do ciclo de vida do projeto, sendo elas: **Problematização**, **concepção e detalhamento da solução** e **projeto e construção de subsistemas da solução proposta**.

**Geral**
- [Arquitetura](docs/geral/anexo.md)
- [Cronograma](docs/geral/cronograma.md)
- [EAP](docs/geral/eap_geral.md)
- [Estimativa de Custos](docs/geral/estimativa-custos.md)
- [Levantamento de riscos](docs/geral/FMEA.md)
- [Requisitos Gerais](docs/geral/requisitos.md)
- [Termo de Abertura](docs/geral/termo_de_abertura_de_projeto.md)
- [Visao geral](docs/geral/visaogeral.md)
- [Integracao de Áreas](docs/geral/integracao.md)

**Software**
- [Arquitetura de software](docs/software/arquitetura.md)
- [Backlog do produto](docs/software/backlog_produto.md)
- [Casos de uso](docs/software/casos-de-uso.md)
- [Diagrama BPMN](docs/software/diagrama-bpmn.md)
- [Diagrama de pacotes](docs/software/diagrama-de-pacotes.md)
- [EAP](docs/software/eap_software.md)
- [Funcionalidades](docs/software/funcionalidades.md)
- [Documento de identidade](docs/software/identidade.md)
- [Prototipo de alta Fidelidade](docs/software/prototipo.md)
- [Requisitos de software](docs/software/requisitos.md)
- [Projeto de subsistema de Software](docs/software/subsistema-software.md)

**Eletrônica/Energia**
- [Arquitetura de Eletrônica](docs/eletronica-energia/arquitetura_eletronica.md)
- [Arquitetura de Energia](docs/eletronica-energia/arquitetura_energia.md)
- [EAP](docs/eletronica-energia/eap.md)
- [Subsistema de Eletrônica](docs/eletronica-energia/Projeto_subsistema_eletronica.md)
- [Subsistema de Energia](docs/eletronica-energia/Projeto_Subsistema_Energia.md)
- [Requisitos](docs/eletronica-energia/requisitos-eletronica-energia.md)

**Estruturas**
- [EAP](docs/estruturas/EAP_estruturas.md)
- [Requisitos](docs/estruturas/Requisitos_estruturas.md)
- [Arquitetura](docs/estruturas/arquitetura_subs_estruturas.md)
- [Subsistema](docs/estruturas/Projeto_subsistema_estruturas.md)
- [Desenhos técnicos](docs/estruturas/DTs.md)
- [Manual de montagem](docs/estruturas/Manual_montagem_estruturas.md)

### C2

Diz respeito à primeira parte da fase 4, a **integração de subsistemas**. Serão feitas:

- Apresentação das adequações realizadas no projeto;
- Demonstração de funcionamento de todos os subsistemas que compõe a solução, já considerando a integração entre as respectivas partes

### C3

Diz respeito à segunda parte da fase 4, a **finalização do produto**. Serão entregues:

- Testes de integração e funcionamento do protótipo de produto;

- Demonstração do funcionamento completo da solução;

- Documentação técnica atualizada nos repositórios do projeto;

- Bannner de apresentação do projeto;

- Vídeo de propaganda da solução, contemplando seu funcionamento completo.