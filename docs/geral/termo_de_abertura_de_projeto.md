# Termo de Abertura do Projeto

## 1. Introdução
<p style="text-align:justify;">O Termo de Abertura do Projeto (TAP) é definido pelo Project Management Body of Knowledge (PMBOK) como um "documento que autoriza formalmente a existência de um projeto, e dá ao seu gerente a autoridade de aplicar recursos organizacionais para as atividades do projeto." (PMI, 2004, 368). Este documento engloba informações envolvendo escopo, orçamento, marcos e partes interessadas do projeto ScanPoint.</p>

## 2. Descrição do Projeto
<p style="text-align:justify;">Este projeto dedica-se a construção de um sistema de digitalização 3D denominado ScanPoint, que simplifica e aprimora o processo de captura e reprodução tridimensional de objetos físicos. O sistema é composto por uma mesa escaner equipada com um Arduino, um sensor infravermelho, e um aplicativo desktop responsável pelo processamento dos dados capturados e a geração de arquivos STL para impressão 3D.</p>

## 3. Justificativa do Projeto
<p style="text-align:justify;">O projeto ScanPoint foi concebido para atender a crescente demanda por soluções acessíveis e eficientes na digitalização 3D, que facilitem a produção de modelos tridimensionais de alta precisão. A atual complexidade e custo elevado dos equipamentos de digitalização 3D limitam seu uso a indústrias específicas, enquanto o ScanPoint visa democratizar essa tecnologia, tornando-a acessível a um público mais amplo, incluindo pequenos fabricantes, educadores e entusiastas.</p>

## 4. Objetivos do Projeto
<p style="text-align:justify;">O objetivo principal do projeto ScanPoint é desenvolver um sistema que permita a captura eficiente de dados de objetos físicos e a geração de modelos 3D prontos para impressão. Isso inclui:</p>

* Proporcionar uma interface intuitiva para os usuários.
* Garantir a precisão e a qualidade dos modelos 3D gerados.
* Tornar o processo de digitalização 3D mais acessível e rápido.
* Oferecer uma solução robusta e confiável para diversas aplicações.

## 5. Requisitos de Alto Nível
* Captura precisa de pontos de distância utilizando sensores infravermelho.
* Processamento otimizado dos dados capturados para gerar modelos 3D de alta qualidade.
* Interface de usuário intuitiva e fácil de usar.
* Pré-visualização do modelo 3D gerado antes da finalização.
* Opção de download do arquivo STL para impressão posterior.

## 6. Restrições
* Dependência de hardware específico, como Arduino e sensores infravermelho.
* Necessidade de um computador para rodar o aplicativo desktop.
* Requerimentos de calibração e precisão dos sensores para garantir a qualidade dos modelos.
* Tamanho e características, como opacidade, do objeto escaneado.

## 7. Cronograma e Marcos 
<p style="text-align:justify;">O projeto terá duração de aproximadamente 120 dias, distribuídos em três principais entregas (pontos de controle). Cada PC possui metas específicas para desenvolvimento e testes das funcionalidades.</p>

| Data | Marco | Descrição |
|---|---|---|
| 03/05/2024 | PC 1 | Entrega da documentação envolvendo a problematização, concepção, detalhamento da solução, projeto e construção de subsistemas da solução proposta. |
| 07/06/2024 | PC 2 | Entrega apresentando as adequações realizadas no projeto e demonstração de funcionamento de todos os subsistemas que compõe a solução, já considerando a integração entre as respectivas partes. |
| 12/07/2024 | PC 3 | Exibição da integração e funcionamento do protótipo de produto, demonstração do funcionamento completo da solução, documentação técnica atualizada nos repositórios do projeto, bannner de apresentação do projeto e vídeo de propaganda da solução contemplando seu funcionamento completo. |

## 8. Orçamento
<p style="text-align:justify;">O orçamento total do projeto é de R$ 494,01 com seus custos divididos entre recursos materiais e equipamentos, fora os que foram herdados das turmas anteriores de Projeto Integrador 2.</p> Para mais detalhes desse processo consultar a [Estimativa de custos](/docs/geral/estimativa-custos.md).

## 9. Riscos
<p style="text-align:justify;">Foram identificados vários riscos para o projeto, categorizados utilizando a análise FMEA. Nessa análise, uma equipe multidisciplinar identifica e classifica as possíveis falhas (modo de falha), analisando suas causas e efeitos (efeito da falha), e estimando a gravidade, frequência de ocorrência e detectabilidade de cada falha. O resultado é uma pontuação de Número de Priorização de Risco (NPR), que permite aos membros priorizar as falhas mais críticas para implementar medidas preventivas ou corretivas.</p> Para mais detalhes, consulte o [Levantamento de Riscos](/docs/geral/FMEA.md).

## 10. Partes Interessadas

| Representantes | Descrição | Tipo | Responsabilidades | Critério de sucesso | Envolvimento |
|---- |---- |---- |---- |---- |---- |
| Brenda Santos, Carla Rocha, Denniel William, Maria Claudia, Carolina Oliveira, Cássio Filho, Diogo Soares, Miguel Munoz, Lucas Pantoja, Ana Carolina, Guilherme Basilio, Artur de Souza, Ciro Costa, Vinicius Vieira, Pedro Menezes | Desenvolvimento do Projeto | Estudantes da Universidade de Brasília, cursando a disciplina de Projeto Integrador 2 | Desenvolvimento, testes e documentação | Finalizar o desenvolvimento e realizar as entregas dentro dos prazos | Alto |
| Alex, Suélia, Carla, Hander e Rafael | Docentes | Professor das disciplina de Projeto Integrador 2 pela Universidade de Brasília | Avaliar e orientar os estudantes no decorrer do projeto | Avaliar o produto em sua totalidade | Alto |

## 11. Referências
* ["PMI - Project Charter"](https://www.pmi.org/learning/library/charter-selling-project-7473)
* ["Quick Guide Project Charter"](https://www.projectmanager.com/blog/project-charter)
 
## Tabela de Versionamento

| Data | Versão| Descrição | Autor |   
|------|-------|-----------|-------|   
|29/03/2024|0.1|Estrutura do Termo de Abertura| Miguel|   
|03/05/2024|1.0|Correções e preenchimento | Carla R. Cangussú|
|04/05/2024|2.0| Inserção de fonte da tabela | Ana Carolina |  
|07/06/2024|3.0| Ajuste geral | Brenda Santos | 
|07/07/2024|2.0| Ajustes de português | Ana Carolina |  