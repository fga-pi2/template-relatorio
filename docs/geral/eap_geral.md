# Estrutura Analítica do Projeto - EAP

## 1. Introdução

<p style="text-align: justify;"> A Estrutura Analítica do Projeto (EAP) é uma ferramenta fundamental no gerenciamento de projetos, que tem a finalidade de organizar e decompor as entregas e tarefas de um projeto em componentes menores e mais gerenciáveis. Representada em forma de diagrama hierárquico, a EAP é apresentada em níveis, começando com as entregas principais do projeto e dividindo-as em sub-entregas e tarefas cada vez menores. </p>

<p style="text-align: justify;"> Essa abordagem possibilita uma visão geral do projeto, permitindo a identificação dos principais entregáveis e das atividades necessárias para a conclusão bem-sucedida do projeto, independentemente do seu tamanho ou complexidade. Além disso, a EAP desempenha um papel crucial no escopo do projeto, na definição de responsabilidades e na estipulação de marcos e entregas essenciais. </p>

## 2. Metodologia

<p style="text-align: justify;"> Esse documento foi elaborado seguindo as diretrizes do plano de ensino da matéria. Com isso, foram definidas as hierarquias de mais alto nível em cada etapa do projeto, orientando nas entregas de cada fase. </p>

## 3. Estrutura

<p style="text-align: justify;"> A EAP é uma árvore que possui três níveis: </p>

* **Nível 1**: O nível 1 da EAP é composto pelas atividades de alto nível do projeto. Essas atividades são chamadas de atividades de projeto. Elas são as atividades que representam o projeto como um todo.

* **Nível 2**: O nível 2 da EAP é composto pelas atividades de nível intermediário do projeto. Essas atividades são chamadas de atividades de pacote. Elas são as atividades que representam um conjunto de atividades de nível mais baixo.

* **Nível 3**: O nível 3 da EAP é composto pelas atividades de baixo nível do projeto. Essas atividades são chamadas de atividades de trabalho. Elas são as atividades que representam as tarefas mais básicas do projeto.

## 4. Planilha EAP

<iframe width="768" height="768" src="https://docs.google.com/spreadsheets/d/1KWgKsgf_MtKPNWU6sujn9LvrJqU3NcNO0QaLYJdHL4Q/edit?usp=sharing" frameborder="0" scrolling="no" allow="fullscreen; clipboard-read; clipboard-write" allowfullscreen></iframe>

<font size="2"><p style="text-align: center">Fonte: Tabela de EAP - Autoria Própria.</p></font>

A partir do EAP gerado para o projeto geral, podemos detalhar as tarefas contidas em cada tópico:

### Concepção

* <p style="text-align: justify;">Definição do Projeto: Formular uma visão clara e detalhada do projeto, incluindo seu propósito e benefícios esperados. Assegurar que todos os envolvidos compreendam a finalidade e a importância do projeto.</p>
* <p style="text-align: justify;">Objetivos: Estabelecer metas específicas, mensuráveis, alcançáveis, relevantes e temporais para o projeto. Fornecer um direcionamento claro e um critério para medir o sucesso do projeto.</p>
* <p style="text-align: justify;">Requisitos: Coletar e documentar os requisitos funcionais e não funcionais do sistema. Garantir que o sistema atenda às necessidades dos usuários e stakeholders.</p>
* <p style="text-align: justify;">Definição de Escopo: Determinar o que está incluído e excluído do projeto, detalhando todas as entregas e os limites do projeto. Evitar mudanças não controladas no escopo durante o desenvolvimento do projeto.</p>
* <p style="text-align: justify;">Cronograma: Desenvolver um cronograma detalhado que inclua todas as atividades do projeto com seus respectivos prazos. Gerenciar o tempo de forma eficiente e assegurar a conclusão do projeto dentro do prazo estipulado.</p>
* <p style="text-align: justify;">Definição de Usuário: Identificar e definir os perfis dos usuários finais que irão interagir com o sistema. Assegurar que o sistema seja desenvolvido com foco nas necessidades e expectativas dos usuários finais.</p>

### Documentação

* <p style="text-align: justify;">EAP (Estrutura Analítica do Projeto): Criar um detalhamento hierárquico de todas as atividades do projeto. Facilitar o planejamento, execução e controle do projeto.</p>
* <p style="text-align: justify;">TAP (Termo de Abertura do Projeto): Elaborar um documento que formaliza a autorização do projeto. Obter o comprometimento e a aprovação formal dos stakeholders.</p>
* <p style="text-align: justify;">Desenhos Mecânicos: Criar desenhos técnicos detalhados dos componentes mecânicos do projeto. Fornecer especificações claras para a fabricação e montagem.</p>
* <p style="text-align: justify;">Diagramas Unifilares/Trifilares de Sistemas de Alimentação: Desenvolver diagramas elétricos que representem a distribuição de energia no sistema. Assegurar a correta implementação e manutenção dos sistemas de alimentação.</p>
* <p style="text-align: justify;">Diagramas Esquemáticos de Circuitos Eletrônicos: Elaborar diagramas que detalham a configuração dos circuitos eletrônicos. Facilitar a montagem e a verificação dos circuitos.</p>
* <p style="text-align: justify;">Diagramas Detalhando Barramentos de Alimentação dos Circuitos Eletrônicos: Criar diagramas que mostram a distribuição de energia nos circuitos eletrônicos. Garantir a correta alimentação e funcionamento dos componentes eletrônicos.</p>
* <p style="text-align: justify;">Diagramas com Detalhes de Lógicas e Protocolos de Comunicação entre Elementos: Desenvolver diagramas que expliquem as lógicas e protocolos de comunicação entre os diversos elementos do sistema. Assegurar a integração e a comunicação eficiente entre os componentes.</p>
* <p style="text-align: justify;">Diagramas de Classes: Criar diagramas que representem a estrutura das classes e suas interações no software. Facilitar o desenvolvimento e a manutenção do código.</p>
* <p style="text-align: justify;">Diagramas de Casos de Uso: Desenvolver diagramas que detalhem as interações entre os usuários e o sistema. Garantir que todos os requisitos funcionais sejam atendidos.</p>
* <p style="text-align: justify;">Diagramas com Protocolos de Comunicação entre Componentes do Software: Elaborar diagramas que expliquem como os diferentes componentes do software se comunicam entre si. Assegurar uma integração eficiente e robusta.</p>
* <p style="text-align: justify;">Requisitos Funcionais e Não Funcionais: Documentar todos os requisitos do sistema, tanto funcionais quanto não funcionais. Garantir que o sistema atenda a todas as especificações necessárias.</p>
* <p style="text-align: justify;">Cronograma: Desenvolver um cronograma detalhado das atividades do projeto. Gerenciar o tempo de forma eficiente e assegurar a conclusão do projeto dentro do prazo estipulado.</p>
* <p style="text-align: justify;">Custos e Orçamentos: Estimar e documentar todos os custos envolvidos no projeto. Assegurar que o projeto seja concluído dentro do orçamento previsto.</p>
* <p style="text-align: justify;">Riscos: Identificar e documentar todos os riscos potenciais do projeto. Desenvolver estratégias para mitigar e gerenciar esses riscos.</p>

### Pré-construção

* <p style="text-align: justify;">Prototipação: Desenvolver protótipos iniciais dos componentes do sistema. Validar conceitos e identificar possíveis problemas antes do desenvolvimento completo.</p>
* <p style="text-align: justify;">Validação: Testar e validar os protótipos desenvolvidos. Assegurar que os protótipos atendem aos requisitos e funcionam conforme esperado.</p>
* <p style="text-align: justify;">Aquisição de Materiais: Identificar e adquirir todos os materiais necessários para a construção do sistema. Garantir que todos os componentes necessários estejam disponíveis para a fase de construção.</p>

### Estrutura

* <p style="text-align: justify;">Arquitetura: Desenvolver a arquitetura do sistema, incluindo todos os componentes e suas interações. Assegurar que a arquitetura atende aos requisitos do projeto.</p>
* <p style="text-align: justify;">Levantamento de Custos: Estimar e documentar os custos relacionados à estrutura do sistema. Assegurar que os custos estejam dentro do orçamento previsto.</p>
* <p style="text-align: justify;">Simulações: Realizar simulações para testar a estrutura do sistema sob diversas condições. Identificar possíveis problemas e otimizar o design.</p>
* <p style="text-align: justify;">Montagem: Montar a estrutura do sistema conforme os desenhos e especificações técnicas. Assegurar que a montagem seja realizada de forma correta e segura.</p>
* <p style="text-align: justify;">Testes de Carga e Estresse: Realizar testes de carga e estresse na estrutura do sistema. Garantir que a estrutura pode suportar as condições operacionais previstas.</p>
* <p style="text-align: justify;">Integração com Eletrônica e Software: Integrar a estrutura física com os componentes eletrônicos e de software. Assegurar que todos os sistemas funcionem de forma integrada e eficiente.</p>
* <p style="text-align: justify;">Testes: Realizar testes finais na estrutura do sistema. Garantir que todos os componentes funcionem conforme esperado e atendam aos requisitos do projeto.</p>

### Eletrônica

* <p style="text-align: justify;">Arquitetura: Desenvolver a arquitetura eletrônica do sistema, incluindo todos os circuitos e componentes. Assegurar que a arquitetura atende aos requisitos do projeto.</p>
* <p style="text-align: justify;">Revisão da Arquitetura: Revisar e validar a arquitetura eletrônica desenvolvida. Garantir que todos os componentes eletrônicos funcionem de forma integrada e eficiente.</p>
* <p style="text-align: justify;">Instrumentação Eletrônica: Desenvolver e implementar a instrumentação eletrônica necessária para o sistema. Assegurar a precisão e confiabilidade das medições.</p>
* <p style="text-align: justify;">Sistema Embarcado: Desenvolver e implementar o sistema embarcado para controlar os componentes eletrônicos. Garantir que o sistema embarcado funcione de forma eficiente e confiável.</p>
* <p style="text-align: justify;">Testes de Comunicação: Realizar testes de comunicação entre os componentes eletrônicos. Garantir que a comunicação seja eficiente e livre de erros.</p>
* <p style="text-align: justify;">Integração com Software: Integrar os componentes eletrônicos com o software do sistema. Assegurar que todos os sistemas funcionem de forma integrada e eficiente.</p>

### Energia

* <p style="text-align: justify;">Consumo de Energia: Analisar e documentar o consumo de energia dos componentes do sistema. Assegurar que o consumo de energia esteja dentro dos limites aceitáveis.</p>
* <p style="text-align: justify;">Definição da Fonte Energética: Identificar e definir a fonte de energia mais adequada para o sistema. Garantir que a fonte de energia selecionada atende aos requisitos do projeto.</p>
* <p style="text-align: justify;">Testes de Eficiência Energética: Realizar testes para avaliar a eficiência energética dos componentes do sistema. Otimizar o consumo de energia e reduzir desperdícios.</p>
* <p style="text-align: justify;">Monitoramento de Desempenho Energético: Monitorar o desempenho energético do sistema durante a operação. Identificar oportunidades de melhoria e otimização.</p>

### Software

* <p style="text-align: justify;">Requisitos de Software: Coletar e documentar todos os requisitos do software. Garantir que o software atenda às necessidades dos usuários.</p>

* <p style="text-align: justify;">Arquitetura do Software: Desenvolver a arquitetura do software, incluindo todos os módulos e suas interações. Assegurar que a arquitetura atende aos requisitos do projeto.</p>

* <p style="text-align: justify;">Desenvolvimento do Aplicativo: Desenvolver o aplicativo desktop para interação com o sistema. Garantir que o aplicativo seja funcional e fácil de usar.</p>

* <p style="text-align: justify;">Integração com a Estrutura e Eletrônica: Integrar o software com a estrutura física e os componentes eletrônicos. Assegurar que todos os sistemas funcionem de forma integrada e eficiente.</p>

* <p style="text-align: justify;">Desenvolvimento do Software Embarcado: Desenvolver o software embarcado para controlar os componentes eletrônicos. Garantir que o software embarcado funcione de forma eficiente e confiável.</p>

* <p style="text-align: justify;">Revisões de Código: Realizar revisões periódicas do código desenvolvido. Garantir a qualidade e a conformidade do código com as melhores práticas de desenvolvimento.</p>

* <p style="text-align: justify;">Testes de Usabilidade: Realizar testes de usabilidade do aplicativo desktop. Assegurar que o aplicativo seja intuitivo e fácil de usar pelos usuários finais.</p>

## 6. Referências

> EUAX. EAP (Estrutura Analítica do Projeto): o que é, como fazer e qual a diferença entre EAP e Cronograma. Disponível em: https://www.euax.com.br/2018/12/eap-estrutura-analitica-projeto/. Acesso em: 12 de Abril de 2024.

> WBS - IBM. Disponível em https://www.ibm.com/docs/en/mea/761?topic=SSXB7Z_7.6.1/com.ibm.meaora.doc/sag/c_ctr_oraclepa_proj_task_integ.html. Acesso em: 12 de Abril de 2024.

> PMI. PMBOK® Guide. Disponível em: https://www.pmi.org/pmbok-guide-standards/foundational/pmbok. Acesso em: 12 de Abril de 2024.

## Versionamento

| Versão | Data       | Modificação                           | Autor                                    |
| ------ | ---------- | ------------------------------------- | ---------------------------------------- |
| 0.1    | 12/04/2024 | Criação do documento                  | Ana Carolina Rodrigues e Denniel William |
| 0.2    | 12/04/2024 | Introdução e Metodologia              | Ana Carolina Rodrigues e Denniel William |
| 0.3    | 12/04/2024 | Adição do link do eap                 | Ana Carolina Rodrigues e Denniel William |
| 1.0    | 29/04/2024 | Adição da planilha do EAP             | Brenda Santos e Denniel William          |
| 1.1    | 03/05/2024 | Alteração da posição do versionamento | Ciro Costa                               |
| 1.2    | 04/05/2024 | Ajustes de alinhamento e formatação   | Ana                                      |
| 2.0    | 07/06/2024 | Ajustes gerais                        | Brenda                                   |
