# Estrutura Analítica do Projeto (EAP) - Revisão

## Introdução

A Estrutura Analítica do Projeto (EAP) é uma metodologia fundamental no gerenciamento de projetos. Ela proporciona uma abordagem sistemática e hierárquica para decompor as entregas e atividades do projeto em componentes menores, facilitando o planejamento, a execução e o controle do projeto.

## Estrutura da EAP

A EAP organiza-se em três níveis principais, garantindo uma visão clara e detalhada das atividades necessárias para a realização do projeto:

- **Nível 1: Atividades Principais do Projeto** - Representa o escopo total do projeto, englobando os objetivos gerais e as entregas chave.

- **Nível 2: Pacotes de Trabalho** - Detalha os conjuntos de atividades relacionadas, que são agrupadas para facilitar a gestão e a atribuição de responsabilidades.

- **Nível 3: Tarefas Detalhadas** - Compreende as ações específicas e mensuráveis necessárias para completar cada pacote de trabalho, permitindo uma estimativa precisa de recursos, tempo e custos.

## Visualização da EAP

A representação gráfica da EAP é realizada através de um diagrama hierárquico, que ilustra todas as atividades do projeto de forma estruturada. Este diagrama é uma ferramenta vital para a comunicação clara das expectativas e responsabilidades entre a equipe de projeto.

<iframe width="768" height="432" src="https://miro.com/app/live-embed/uXjVKPCjfcY=/?moveToViewport=-1332,392,1982,1149&embedId=653482143341" frameborder="0" scrolling="no" allowfullscreen></iframe>

_Centro: Figura 1 - Diagrama da EAP de Energia. Fonte: Elaboração própria._

## Conclusão

A implementação eficaz da EAP é crucial para o sucesso do projeto, assegurando que todas as atividades sejam identificadas, planejadas e executadas de maneira eficiente. Ela serve como a espinha dorsal para o planejamento detalhado do projeto, contribuindo significativamente para a sua conclusão dentro do escopo, prazo e orçamento estabelecidos.

## Referências

- EUAX. "EAP (Estrutura Analítica do Projeto): o que é, como fazer e qual a diferença entre EAP e Cronograma". Disponível em: https://www.euax.com.br/2018/12/eap-estrutura-analitica-projeto/. Acesso em: 12 de abril de 2024.

## Histórico de Revisões

| Versão | Data       | Descrição                       | Autor(a)      |
|--------|------------|---------------------------------|---------------|
| 0.1    | 02/05/2024 | Documento inicial criado.       | Brenda Santos |
| 0.2    | 03/05/2024 | Atualização dos desenhos        | Diogo Soares  |
| 0.3    | 03/05/2024 | Adição de versionamento         | Ciro Costa    |
| 0.4    | 04/05/2024 | Ajustes de alinhamento e fontes | Ana Carolina  |
| 0.5    | 12/07/2024 | Ajustes finais do arquivo       | Pedro Menezes |
