## Requisitos do Subsistema de Energia

Os requisitos definidos para o subsistema de energia visam assegurar uma operação eficaz, confiável e segura do scanner, satisfazendo integralmente as demandas dos usuários.

### Requisitos Funcionais (RF)

| Requisito | Nome                                  | Descrição                                                                                                               |
|-----------|---------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
| RF01      | Integração do Motor e Controle de Velocidade | Desenvolver um sistema de controle para o motor da mesa giratória, permitindo ajustar e manter a velocidade de rotação e interromper a rotação conforme necessário durante o escaneamento. |
| RF02      | Integração do Motor e Ajuste de Altura do Sensor | Desenvolver um sistema de controle para o motor responsável pela regulação de altura do sensor de distância infravermelho, garantindo um posicionamento preciso do sensor em relação ao objeto escaneado. |
| RF03      | Comunicação com o Software            | Implementar um sistema de comunicação eficiente entre os controles dos motores e da iluminação com o software de escaneamento. |
| RF04      | Alimentação do Sistema                | Projetar um sistema de alimentação elétrica que forneça energia estável e adequada para todos os componentes eletrônicos do scanner, incluindo motores, sensor de distância infravermelho e circuitos de controle. |

### Requisitos Não Funcionais (RNF)

| Requisito | Nome                                  | Descrição                                                                                                               |
|-----------|---------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
| RNF01     | Eficiência Energética                 | Otimizar o consumo de energia do sistema através da seleção de componentes eficientes e desenvolvimento de algoritmos de controle energético, visando a maximização da eficiência e a minimização do desperdício. |
| RNF02     | Compatibilidade com Normas de Segurança | Assegurar a segurança dos usuários e a conformidade com regulamentações aplicáveis, seguindo as normas de segurança elétrica relevantes. |
| RNF03     | Tempo de Resposta                     | Minimizar a latência entre os comandos do software e as ações de controle do motor, garantindo uma operação ágil e responsiva do sistema. |
| RNF04     | Durabilidade e Confiabilidade         | Selecionar componentes de alta qualidade e projetar circuitos robustos para assegurar a durabilidade e a confiabilidade do sistema, minimizando a probabilidade de falhas. |
| RNF05     | Compatibilidade com Protocolos de Comunicação | Utilizar protocolos de comunicação padrão e amplamente suportados para facilitar a interoperabilidade entre os diferentes componentes eletrônicos do sistema. |

### Versionamento

| Versão | Data       | Descrição                                              | Responsável   |
|--------|------------|--------------------------------------------------------|---------------|
| 0.1    | 21/04/2024 | Adição dos requisitos iniciais.                        | X      |
| 0.2    | 26/04/2024 | Revisão conforme ajustes acordados pelo grupo.         | X      |
| 0.3    | 03/05/2024 | Inclusão de desenvolvimentos e correção de formatação. | X      |
| 0.4    | 04/05/2024 | Ajustes de fontes e apresentação.                      | X |
| 0.5    | 12/07/2024 | Ajustes finais do arquivo.                             | X |