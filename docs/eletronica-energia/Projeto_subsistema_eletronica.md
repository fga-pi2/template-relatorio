# Projeto do Subsistema de Eletrônica


## 1. Motor de Passo


### 1.1. Estrutura do Motor de Passo
<p style="text-align:justify;">
As características do Motor de Passo, no que diz respeito à velocidade, torque e resolução são definidas primeiramente pela estrutura do motor, então é importante conhecer os aspectos internos e a função das peças que compõem o motor.
</p>

![alt text](../assets/eletronica-energia/image-3.png)

<font size="2"><p style="text-align: center">Figura 1: Estrutura interna do motor de passo <a href="#ref-1"> [1]. </a></p></font>

#### 1.1.1. Rotor

![alt text](../assets/eletronica-energia/image-4.png)

#### 1.1.2. Estator


### 1.3. Funcionamento de um motor de passo
<p style="text-align:justify;">
O motor de passo é um dispositivo eletromecânico composto por bobinas (indutores) e um rotor magnético (ímã permanente ou imantado). As bobinas convertem energia elétrica em campo magnético, alinhando o rotor a esse campo. A excitação correta das bobinas faz o rotor, que gira em torno de seu eixo, mover-se no sentido desejado, convertendo energia magnética em movimento (energia mecânica). O rotor é rotacionado em pequenos incrementos angulares, chamados de "passos" <a href="#ref-2"> [2]</a>.

O funcionamento básico do motor de passo envolve o uso de bobinas alinhadas dois a dois. Quando energizadas, elas atraem o rotor, alinhando-o com o campo magnético gerado, resultando em uma variação angular pequena, chamada de passo. A velocidade e o sentido de movimento são determinados pela forma como cada bobina é ativada, incluindo a ordem e a velocidade entre cada processo de ativação <a href="#ref-2"> [2]</a>.

As vantagens do motor de passo incluem sua precisão de posicionamento e torque aplicado, além de ter uma resposta excelente na aceleração e desaceleração, devido à sua lógica digital. No entanto, ele pode apresentar baixo desempenho em alta velocidade, requer um certo grau de complexidade para operação e pode sofrer ressonância devido a um controle inadequado <a href="#ref-2"> [2]</a>.

Complementando com o banco de dados, o motor de passo é frequentemente utilizado em aplicações que exigem precisão de posicionamento, como impressoras 3D, máquinas CNC e robótica industrial. Ele oferece um controle preciso sobre o movimento, o que é essencial em muitas aplicações. Além disso, o motor de passo pode ser facilmente controlado por microcontroladores, o que o torna uma escolha popular em projetos eletrônicos e de automação <a href="#ref-2"> [2]</a>.

A figura a seguir mostra como um motor de passo pode ser controlado mediante lógica binária:
</p>
 
![alt text](../assets/eletronica-energia/image-12.png)
<font size="2"><p style="text-align: center">Figura 9: Motor de passo controlado pelas entradas To até T3 <a href="#ref-2"> [2]</a>.</p></font>

## 2. Placa Arduino Uno
<p style="text-align:justify;">
O Arduino Uno é uma placa microcontrolada baseada no chip ATmega328. Esta placa é muito utilizada no desenvolvimento de projetos embarcados, é hoje em dia é a mais utilizada e documentada da família Arduino.
</p>

![alt text](../assets/eletronica-energia/image-13.png)   

<font size="2"><p style="text-align: center">Figura 10: Placa Arduino Uno, diagrama de barramento e diagrama esquemático. Fonte: Autoria própria.</p></font>

<p style="text-align:justify;">
A placa conta com um chip ATmega328 que é um microcontrolador único desenvolvido pela Atmel, pertencente à família megaAVR. A arquitetura do Arduino Uno segue o padrão Harvard e possui um núcleo de processador RISC de 8 bits customizado.
</p>

![alt text](../assets/eletronica-energia/image-14.png)

<font size="2"><p style="text-align: center">Figura 11: Diagrama de pinos da placa Arduino Uno <a href="#ref-3"> [3]</a>.</p></font>

 
### 2.1. Características principais do Arduino Uno



##### 2.2.1.2. Entradas e saídas digitais
<p style="text-align:justify;">
O Arduino Uno possui 14 pinos que podem ser usados como entrada ou saída a depender da necessidade do programador. Com a ajuda de funções como pinMode(), digitalWrite() e Digital Read() é possível definir como os pinos serão utilizados <a href="#ref-3"> [3]</a>.
</p>


 


 
### 2.3. Memória
<p style="text-align:justify;">
A memória do microcontrolador Atmega328 do Arduino Uno inclui memória flash de 32 KB para armazenamento de código, SRAM-2 KB e EEPROM-1 KB <a href="#ref-3"> [3]</a>.
</p>

### 2.4. Comunicação
<p style="text-align:justify;">
O Arduino Uno ATmega328 oferece comunicação serial UART TTL e é acessível em pinos digitais como TX (1) e RX (0). O software de um Arduino possui um monitor serial que permite dados fáceis. Existem dois LEDs na placa, como RX e TX, que piscarão sempre que os dados estiverem sendo transmitidos através do USB.
</p>





## 3. Modulação PWM



## 4. Driver Motor de Passo A4988


## Tabela de versionamento

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 1.0 | 03/05/2024 | Subindo o arquivo com dados do motor de passo | Miguel Munoz |
| 1.2 | 03/05/2024 | Atualização do documento de acordo com o template | Miguel Munoz |
| 1.3 | 03/05/2024 | Revisão da Diretoria Técnica para padronização | Carolina |
| 1.4 | 04/05/2024 | Ajustes de fontes e alinhamentos | Ana Carolina |
| 1.5 | 04/05/2024 | Atualização da formatação | Carolina |
| 2.0 | 05/06/2024 | Atualização das imagens | Miguel |
| 2.1 | 05/06/2024 | Ajuste do texto e numeração das imagens | Miguel |
| 2.2 | 06/06/2024 | Finalização da Informação de Eletrônica | Miguel |
| 2.3 | 07/06/2024 | Atualização da formatação e revisão do texto | Carolina |
| 2.4 | 01/07/2024 | Atualização de sensor de alcance e resumo | Miguel |
| 2.5 | 01/07/2024 | Finalização Eletrônica | Miguel |
| 3.0 | 12/07/2024 | Revisão para entrega final | Carolina |