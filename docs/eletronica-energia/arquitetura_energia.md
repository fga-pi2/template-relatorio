# Arquitetura do Subsistema de Energia

A arquitetura do subsistema de energia é fundamental para garantir a alimentação elétrica adequada e eficiente do produto 

![Diagrama de Energia](../assets/eletronica-energia/Arquitetura_de_Subs_Energia.jpeg)

_Figura 1 - Arquitetura de Energia. Fonte: Autoria própria [1]._

## Versionamento

| Versão | Data       | Descrição                                   | Responsável   |
|--------|------------|---------------------------------------------|---------------|
| 1.0    | 28/04/2024 | Documento inicial criado.                   | Lucas Pantoja |
| 1.1    | 29/04/2024 | Revisão do texto e atualização da imagem.   | Lucas Pantoja |
| 1.2    | 03/05/2024 | Atualização para padronização do documento. | Lucas Pantoja |
| 1.3    | 04/05/2024 | Ajustes de alinhamento e formatação.        | Ana Carolina  |
| 1.4    | 12/07/2024 | Ajustes finais do arquivo.                  | Pedro Menezes |