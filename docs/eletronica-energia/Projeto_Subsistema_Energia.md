# Projeto do Subsistema de Energia

<p style="text-align:justify;">
O subsistema de energia é responsável pela alimentação dos demais subsistemas, garantindo eficiência energética e capacidade de operação. Portanto, o foco da equipe consistiu em dimensionar os gastos energéticos, selecionar componentes eficazes, de baixo consumo e implementar estratégias de gerenciamento de energia em conformidade com as normas de segurança aplicáveis, assegurando a proteção dos usuários e a conformidade com os padrões de qualidade e regulamentações pertinentes.
</p>

## 1. Levantamento de carga
<p style="text-align:justify;">
Com o intuito de determinar as características energéticas do projeto, realizou-se uma análise do consumo estimado dos dispositivos. Para uma margem de segurança foi adicionado  20%  sobre a potência total do projeto. Assim, temos o levantamento:
</p>

<font size="2"><p style="text-align: center">Tabela 1: Levantamento de carga.</p></font>


| Componente               | Qtd | Corrente [A]|Tensão [V] |Potência [W]|
| ------                   | --- | ----------- | --------- | ---------- |
|Driver A4988              |  2  |  2,00       | 12,00     | 48,00      |
|Motor                     |  2  |  0,570      | 6,80      | 7,752      |
|Potência Total [W]        |     |             |           | 55,752     |
|Potência Total com 20% [W]|     |             |           | 66,902     |

## 2. Bateria
<p style="text-align:justify;">
Para o projeto ScanPoint, está estimado em utilizar duas baterias de  12 V de 7 Ah, cada.
</p>

![Bateria](../assets/eletronica-energia/bateria_unipower.jpg)

<font size="2"><p style="text-align: center">Figura 1 - Bateria UP1270SEG.</p></font>

<font size="2"><p style="text-align: center">Tabela 2 - Caracteríticas da bateria.</p></font>


| Marca                             | UNICOBA       |
|------                             | ------        |
|    Modelo                         |UP 1270 SEG    |
|Composição das células da bateria  |Chumbo - Ácido |
|Voltagem                           | 12 V          |
|    Amperagem                      |    7 Ah       |
|    Dimensões [cm]                 |   8 x 16 x 11 |
|    Peso [kg]                      |    2          |




## 3. Cabos

## 4. Barramento

<p style="text-align:justify;">
Com a finalidade de ramificar as ligações e assim alimentar os dispositivos do projeto, são utilizados dois barramentos de 80 mm. Um barramento é utilizado para as ligações com a fiação positiva, identificada com cabos de coloração avermelhada, e o outro para a fiação negativa, identificado com cabos da cor preta.
</p>

![barramento](../assets/eletronica-energia/BARRAMENTO.jpg)

<font size="2"><p style="text-align: center">Figura 6 - Barramento 80 mm.</p></font>

## 5. Plugues P4

## 6. Carregador da Bateria

<p style="text-align:justify;">
Utilizar um carregador adequado visa garantir a longevidade e o desempenho da bateria. Um carregador que fornece uma corrente inadequada pode comprometer a bateria ocasionando danos, reduzindo a vida útil da bateria ou não carregá-la de maneira eficiente. Os carregadores modernos geralmente seguem normas específicas de segurança, como a ABNT NBR IEC 60335-2-29, que garantem que os dispositivos operem de maneira segura e eficiente.
</p>


## 7. Interruptores
<p style="text-align:justify;">
Para o controle do fornecimento de energia para o subsistema de eletrônica, foram utilizados dois botões interruptores, um para cada bateria. Isso permite fornecer ou interromper a energia conforme a necessidade do momento, visando maior praticidade e evitando o desperdício de energia das baterias e o superaquecimento dos componentes eletrônicos.
</p>

Especificações do Produto:
- Dimensões (Comprimento x Altura x Largura): 20 mm x 20 mm x 20 mm
- Corrente Nominal: 10 A
- Materiais: Plástico e Metal
- Quantidade de Canais: 2


![Botão liga-desliga](../assets/eletronica-energia/minibotao.png)

<font size="2"><p style="text-align: center">Figura 9 - Interruptor "liga-desliga".</p></font>



## 8. Diagrama Unifilar



## 9. Montagem

<p style="text-align:justify;">
Antes de iniciar o processo de montagem do subsistema de energia, foi desenvolvido pela equipe um diagrama de montagem que foi revisado e aprovado pelo docente responsável. Também foram revisitadas as normas técnicas relacionadas à solução.
</p>




## Referências Bibliográficas

[1] Associação Brasileira de Normas Técnicas (ABNT). NBR 5410: Instalações elétricas de baixa tensão. Rio de Janeiro, 2004.

<div id="ref-2"/>
[2] Instituto Nacional de Meteorologia (INMET): Clima. Prognóstico de Tempo (2024). Acesso em: 27 abr. 2024. Disponível em: https://clima.inmet.gov.br/progt.

[3] ABNT NBR IEC 60335-2-29:2010. Aparelhos eletrodomésticos e similares - Segurança - Parte 2-29: Requisitos particulares para carregadores de baterias. Rio de Janeiro: ABNT, 2010.

## Tabela de versionamento

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 1.0 | 28/04/2024 | Criação do documento | X |
| 1.1 | 29/04/2024 | Edição de textos, tabelas e imagens | X |
| 1.2 | 01/05/2024 | Modificação da tabela 1 | X |
| 1.3 | 03/05/2024 | Modificação de texto para padronização | X |
| 1.4 | 03/05/2024 | Revisão da Diretoria Técnica | X |
| 1.5 | 04/05/2024 | Ajustes de alinhamento e fontes | Ana X |
| 1.6 | 02/06/2024 | Modificação de imagens e inserção de tópicos (LM317 e barramento) | X |
| 1.7 | 03/06/2024 | Revisão dos novos tópicos e inserção do tópico "Montagem" | X |
| 1.8 | 06/06/2024 | Adição da imagem da montagem | X |
| 1.9 | 07/06/2024 | Criação do tópico "Carregador da Bateria" | X |
| 2.0 | 07/06/2024 | Revisão final para o PC 2 | X |
| 2.1 | 12/07/2024 | Adequação do texto para a entrega final | X |
| 2.2 | 12/07/2024 | Modificação do Power Budget e Diagrama Unifilar atualizado | X |

