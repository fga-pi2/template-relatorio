# Projeto do Subsistema de Estruturas

## Materiais

### Definição de critérios de avaliação dos materiais



<p style="text-align: justify;">
Na tabela abaixo, estão listados os critérios definidos as pontuações para cada requisito e por fim o peso definido pela soma das pontuações:
</p>

| Critério                                    | RF1 | RF2 | RF3 | RF4 | RN1 | RN2 | Peso |
|---------------------------------------------|-----|-----|-----|-----|-----|-----|------|
| Possui normas nacionais para especificar seu uso? |  1  |  1  |  1  |  1  |  0  |  0  |  4   |
| Possui embasamento literário extenso para os cálculos? |  1  |  1  |  1  |  1  |  0  |  0  |  4   |
| Objeto disponível no laboratório?           |  0  |  0  |  0  |  0  |  1  |  1  |  2   |
| Possui vasta aplicação industrial no contexto de estruturas? |  1  |  1  |  1  |  1  |  1  |  1  |  6   |

<div align="center">
    Tabela 1 - Critérios para cada requisito de estruturas<br />
    Fonte: Autores. 
</div>
<br />

### Lista e definição de materiais

## Análise Estrutural Preliminar



### Ensaios Estruturais


<p style="text-align: justify;">
Graças aos pontos descritos nos tópicos anteriores, optou-se pela realização de um ensaio para avaliação de integridade estrutural simples, onde uma peça de testes foi elaborada e submetida a diversas quantidades de cargas com o intuito de validar seu uso para a aplicação desejada no projeto. A peça de estudo foi fabricada a partir do material PLA (biopolímero ácido poliláctico), com configuração de densidade de preenchimento de 10% do tipo zigue-zague e com 4 paredes de 0,88mm de espessura.
</p>

![Geometria de preenchimento da peça](imagens/Proj_Subs_Estruturas_Ensaios_geometria_.jpeg)

<p style="text-align:center;">
Figura 1 - Geometria de preenchimento da peça <br />
Fonte: Autores.
</p>

### Simulações numéricas

#### Suporte das Placas 


#### Placa do Fundo da Caixa

# Referências

* KRAJEWSKI, Lee J.; RITZMAN, Larry P.; MALHOTRA, Manoj K. Administração de produção e operações. 8. ed. São Paulo: Pearson, 2012. xiv, 615 p. ISBN 9788576051725.

* ASSOCIAÇÃO BRASILEIRA DE NORMAS TÉCNICAS. ABNT NBR 08403: Aplicação de linhas em desenhos - Tipos de linhas - Larguras das linhas Rio de Janeiro, 1984.

* ASSOCIAÇÃO BRASILEIRA DE NORMAS TÉCNICAS. ABNT NBR 10067 - Princípios Gerais de Representação em Desenho Técnico, 1995

* ASSOCIAÇÃO BRASILEIRA DE NORMAS TÉCNICAS. ABNT NBR 10068 - Folha de Desenho - Leiaute e Dimensões, 1987
 
* ASSOCIAÇÃO BRASILEIRA DE NORMAS TÉCNICAS. ABNT NBR 10126 - Cotagem em Desenho Técnico, 1987

## Versionamento
| Versão | Data | Modificação | Autor |
|--|--|--|--|
| 0.1 | 30/04/2024 | Criação do documento | X |
| 0.2 | 02/05/2024 | Adicionando arquivos de simulação | X|
| 1.0 | 03/05/2024 | Atualizações finais |X|
| 2.0 | 07/06/2024 | Atualizações  |X |
