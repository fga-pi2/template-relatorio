# Requisitos de Estruturas

## Requisitos Funcionais (RF)

<p style="text-align: justify;">
Os requisitos funcionais da área de estruturas abrangem os aspectos físicos e mecânicos do projeto. Entre eles, tem-se aspectos relacionados a carga do objeto que será escaneado, a movimentação de partes móveis e exigências quanto a sua mobilidade e propriedades mecânicas.
</p>

| Requisito | Nome | Descrição |
|:---------:| :--- |:------------:|
| RF01 | Suportar uma peça escaneável de 2 Kg | A estrutura geral e principalmente do suporte deve comportar a carga prevista da peça a ser escaneada.
| RF02 | Movimentar o sensores | A estrutura deve apresentar partes móveis para a fixação do sensor.
| RF02 | Rotacionar a peça | A estrutura de suporte para a peça deve ser móvel em seu eixo de rotação.
| RF03 | Apresentar estabilidade e resistência | Os componentes e materiais utilizados na montagem devem considerar os movimentos previstos e cargas aplicadas a estrutura.

<div align="center">
    Tabela 1 - Requisitos funcionais 
</div>
<br />

## Requisitos Não Funcionais (RNF)

<p style="text-align: justify;">
Os requisitos não funcionais abragem aspectos relacionados a características do projeto quanto a sua estrutura, podendo ser visualizados na tabela 2. Foram analisados pontos como dificuldade de montagem, fornecimento e acessibilidade do material e segurança do usuário. 
</p>


## Versionamento
| Versão | Data | Modificação | Autor |
|--|--|--|--|
| 0.1 | 19/04/2024 | Criação do documento | X|
| 0.2 | 26/04/2024 | Atualizações |X|
| 1.0 | 03/05/2024 | Entrega | X|