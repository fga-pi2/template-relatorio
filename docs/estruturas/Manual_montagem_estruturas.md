# Manual de Montagem

## Etapa 1


![1](imagens/Manual_montagem_1.png)


<p style="text-align: justify;">
Note no detalhamento que os perfis destacados em marrom devem ser posicionados internamente, já os demais são posicionados externamente na estrutura.
</p>

## Etapa 2



## Versionamento
| Versão | Data | Modificação | Autor |
|--|--|--|--|
| 1.0 | 07/06/2024 | Entrega | x |