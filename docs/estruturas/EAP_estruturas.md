# Estrutura Analítica do Projeto - EAP 

<p style="text-align: justify;">
A Estrutura Analítica do Projeto (EAP) da área de Estruturas irá descrever as atividades a serem desenvolvidas em ordem de execução e a partir das entregas espituladas nos pontos de controle, como é possível visualizar na figura 1.
</p>

<iframe width="768" height="432" src="https://miro.com/app/live-embed/uXjVKPCjfcY=/?moveToViewport=-528,-1191,2108,966&embedId=338951566821" frameborder="0" scrolling="no" allow="fullscreen; clipboard-read; clipboard-write" allowfullscreen></iframe>

<p style="text-align:center;">
    Figura 1 - EAP de Estruturas <br />
    Fonte: Autores.
</p>


## Versionamento
| Versão | Data | Modificação | Autor |
|--|--|--|--|
| 1.0 | 25/04/2024 | Criação do EAP de Estruturas | x |

