# Backlog do Produto




## Épicos


### Épico: Interface (E01)



## Story map

# Roadmap

A partir do backlog do produto, planejamento das sprints e entregas esperadas em cada release

Link para o kanban no gitlab




## Referências

* PEREIRA, P.; TORREÃO, P.; MARÇAL, A. S. Entendendo Scrum para gerenciar projetos de forma ágil. Mundo PM, v. 1, p. 3-11, 2007.

## Tabela de versionamento

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 1.0 | 28/04/2024 | Criação do documento | X |
| 1.1 | 03/05/2024 | Refatoração do documento | X |
| 1.2 | 04/05/2024 | Ajuste no alinhamento e fontes | X |
