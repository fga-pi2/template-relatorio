# Projeto de Subsistema de Software

## Arquitetura de Software

## Interface

### Guia de estilo

### Protótipo de alta fidelidade


### Tecnologia utilizada



## Comunicação com arduíno

## Software embarcado

## Referências
<div id="ref1"/>
>[1][What is Electron? Acesso em 28 de abril de 2024.](https://www.electronjs.org/docs/latest/)

<div id="ref2"/>
>[2] [Diagrama de arquitetura de software. Acesso em 03 de abril de 2024.](https://app.diagrams.net/#G152ySAVfwL1QUKcdnSeAaXdJ9Q1nRN_6T#%7B"pageId"%3A"C2fSY1v2SiZeoUbDoYyL"%7D..)

## Tabela de versionamento

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 0.1 | 29/04/2024 | Criação do documento | x |
| 0.2 | 30/04/2024 | Adição da comunicação com arduíno | x |
| 0.3 | 30/04/2024 | Adição da tecnologia utilizada | x |
| 0.4 | 02/05/2024 | Formatação e adição do software embarcado | x |
| 0.5 | 04/05/2024 | Mudança de titulo do documento e erros de portugues | x |
| 0.6 | 11/07/2024 | Correção de links e erros de português | x |
