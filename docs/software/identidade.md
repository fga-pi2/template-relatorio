# Documento de Identidade

## Visão geral do produto

## Logo e Identidade Visual


## Cores


## Tipografia


## Ícones e Elementos Gráficos


## Layout e Grid:




# Documento de Identidade visual

# Protótipo de alta fidelidade


## Tela inicial


## Tela de processamento


## Tela de erro




## Sobre


## Tabela de versionamento

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 0.1 | 27/04/2024 | Criação da estrutura do documento | x |
| 0.2 | 27/04/2024 | Feito até Ícones e Elementos Gráficos | x |
| 0.3 | 29/04/2024 | Finalização do documento | x |
| 0.4 | 29/04/2024 | Ajuste de formatação | x |
| 0.5 | 29/04/2024 | Ajuste no caminho de algumas imagens | x |
