# Fluxos de Usuário

## Fluxograma do usuario  no sistema (fluxos felizes)

## Fluxograma do usuário no sistema (fluxos de exceção)


## Referências


## Tabela de versionamento

| Versão| Data | Descrição | Responsável|
|-------|------|-----------|------------|
| 1.0 | 12/04/2024 | Criação do documento | X |
| 1.1 | 27/04/2024 | Atualização do documento |  X |
| 1.2 | 03/05/2024 | Organização de fontes e referências | X |
| 1.3 | 04/05/2024 | Ajustes de formatação e alinhamento | X |
| 1.4 | 12/07/2024 | Inclusão da nuvem de pontos | X |
